from inventory.models import Inventory
from user.models import User
from rest_framework import viewsets, permissions
from .serializers import InventorySerializer, UserSerializer

class InventoryViewSet(viewsets.ModelViewSet):
    queryset = Inventory.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = InventorySerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = UserSerializer