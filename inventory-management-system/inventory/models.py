from django.db import models
from user.models import User

class Inventory(models.Model):
    name = models.CharField(max_length=255, null=False)
    owner_id = models.ForeignKey(User, blank=False, null=True, on_delete=models.SET_NULL)
    ipv4address = models.GenericIPAddressField(null=False, unique=True)
    imv6address = models.GenericIPAddressField(null=True, unique=True)
    macaddress = models.CharField(max_length=50, null=True)
    osname = models.CharField(max_length=50, default="unknown")
    osfamily = models.CharField(max_length=50, default="unknown")
    installeddate = models.DateField(auto_now_add=True)
    installedby = models.CharField(max_length=50)
    discovereddate = models.DateField(auto_now_add=True)
    discoveredby = models.CharField(max_length=50)
    modifieddate = models.DateTimeField(auto_now_add=True)
    modifiedby = models.CharField(max_length=50)
    version = models.IntegerField(default=0)
    comment = models.TextField()
