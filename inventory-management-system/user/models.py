from django.db import models

class User(models.Model):
    firstname = models.CharField(max_length=50, null=True)
    lastname =  models.CharField(max_length=50, null=True)
    username =  models.CharField(max_length=50, null=False, blank=False)
    badgeid = models.IntegerField()
    #role_id = models.ForeignKey(Role)
    #usercredential_id
    isactive = models.BooleanField(default=True)
    createddate =  models.DateField(auto_now_add=True)
    createdby = models.CharField(max_length=50, null=True)
    version = models.IntegerField(default=0)